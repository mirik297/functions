const getSum = (str1, str2) => {
  if(typeof str1 != "string" || typeof str2 != "string" || /[a-zа-яё]/i.test(str1) || /[a-zа-яё]/i.test(str2)){
    return false;
  }
  let num1= Number(str1), num2 = Number(str2)
  num1+=num2
  return String(num1);
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  if(typeof authorName!='string'){
    return "Post:0,comments:0";
  }
  let posts=0, numComments=0
  for(let post of listOfPosts){
    if(post['author']==authorName){
      posts++
    }
    let comments=post['comments']
    if(comments!=undefined){
    comments.forEach(comment =>{
      if(comment["author"]==authorName){
        numComments++
      }
    })}
  }
  return "Post:"+posts+",comments:"+numComments;
}

const tickets=(people)=> {
  let quarters=0, halfs=0

  for(let personMoneyAmount of people){
    if(personMoneyAmount == 25){
      quarters++
    }else if(personMoneyAmount == 50){
      if(quarters>0){
        quarters--
        halfs++
      }else{
        return 'NO'
      }
    }else{
      if(quarters*25+halfs*50<personMoneyAmount){
        return 'NO'
      }else{
        quarters--
        halfs--
      }
    }

  }

  return 'YES';
}


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
